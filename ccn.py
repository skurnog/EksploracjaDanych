from __future__ import print_function

import numpy as np

from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.utils import to_categorical
from keras.layers import Dense, GlobalMaxPooling1D
from keras.layers import Conv1D, MaxPooling1D, Embedding
from keras.models import Sequential
from sys import argv
import sys
import os
import pickle

from logger import Logger

DUMP_DIR_REL_PATH = 'dataset_parsed'

MAX_SEQUENCE_LENGTH = 1000
VALIDATION_SPLIT = 0.2
MAX_NB_WORDS = 20000

if len(argv) >= 8:
    SIZE_RATIO = float(argv[1])
    EMBEDDING_DIM, F1, P1, F2, P2, F3 = map(int, argv[2:])
else:
    print('Not enough arguments, using defaults')
    SIZE_RATIO, EMBEDDING_DIM, F1, P1, F2, P2, F3 = (1, 100, 5, 5, 5, 5, 5)


def load_dump(dataset_dump_dir, tag_name, test_label):
    print('Loading data for %s' % test_label if test_label is not None else dataset_dump_dir + '/' + tag_name)

    dump_base_dir = os.path.join(DUMP_DIR_REL_PATH, dataset_dump_dir)
    dump_tagname_dir = os.path.join(DUMP_DIR_REL_PATH, dataset_dump_dir, tag_name)

    texts_fname = os.path.join(dump_base_dir, 'texts.dump')
    texts_f = open(texts_fname, 'rb')
    texts = pickle.load(texts_f)

    labels_fname = os.path.join(dump_tagname_dir, 'labels.dump')
    labels_f = open(labels_fname, 'rb')
    labels = pickle.load(labels_f)

    labels_index_fname = os.path.join(dump_tagname_dir, 'labels_index.dump')
    labels_index_f = open(labels_index_fname, 'rb')
    labels_index = pickle.load(labels_index_f)

    print('Data loaded successfully')
    print('texts: %d, labels count %d\n%s' % (len(texts), len(labels_index), labels_index))

    return labels, labels_index, texts


def cnn(texts, labels, labels_index, epochs):
    print("Tokenizing texts...")
    tokenizer = Tokenizer(num_words=MAX_NB_WORDS)
    tokenizer.fit_on_texts(texts)
    sequences = tokenizer.texts_to_sequences(texts)
    word_index = tokenizer.word_index
    print('Found %s unique tokens.' % len(word_index))

    data = pad_sequences(sequences, maxlen=MAX_SEQUENCE_LENGTH)

    labels = to_categorical(np.asarray(labels))
    print('Shape of data tensor:', data.shape)
    print('Shape of label tensor:', labels.shape)

    print('Preparing data')
    np.random.seed(245837149)

    # split the data into a training set and a validation set
    indices = np.arange(data.shape[0])
    np.random.shuffle(indices)
    print(indices)
    data = data[indices]
    labels = labels[indices]

    size = int(data.shape[0] * SIZE_RATIO)

    num_validation_samples = int(VALIDATION_SPLIT * size)
    num_train_samples = int((1 - VALIDATION_SPLIT) * size)

    x_train = data[:num_train_samples]
    y_train = labels[:num_train_samples]
    x_val = data[-num_validation_samples:]
    y_val = labels[-num_validation_samples:]

    # train a 1D convnet with global maxpooling
    print("Preparing model")
    model = Sequential()
    model.add(Embedding(len(word_index) + 1, EMBEDDING_DIM, input_length=MAX_SEQUENCE_LENGTH))
    model.add(Conv1D(128, F1, activation='relu'))
    model.add(MaxPooling1D(P1))
    model.add(Conv1D(128, F2, activation='relu'))
    model.add(MaxPooling1D(P2))
    model.add(Conv1D(128, F3, activation='relu'))
    model.add(GlobalMaxPooling1D())
    model.add(Dense(128, activation='relu'))
    model.add(Dense(len(labels_index) + 1, activation='softmax'))

    print("Compiling model")
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['acc'])

    print("Training model")
    model.fit(x_train, y_train, batch_size=128, epochs=epochs, validation_data=(x_val, y_val))


def train_validate(dataset_dump_dir, tag_name, test_label=None, epochs=10):
    sys.stdout = Logger('%s-%s%s.log' %
                        (dataset_dump_dir, tag_name, '-%s' % test_label if test_label is not None else ''))

    labels, labels_index, texts = load_dump(dataset_dump_dir, tag_name, test_label)
    print('Training on %d texts' % len(texts))
    cnn(texts, labels, labels_index, epochs)


# TEST
if __name__ == '__main__':
    train_validate('blogs_utf8', '2', 'cnn3_category_e2', epochs=2)
