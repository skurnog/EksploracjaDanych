NASTEPNY PLIK
Processing text dataset
Found 1371 texts.
Loss 2.599375548319109
Accuracy : 0.5587162655%
             precision    recall  f1-score   support

    algeria       0.00      0.00      0.00         1
     angola       0.00      0.00      0.00         1
  argentina       0.00      0.00      0.00         6
      aruba       0.00      0.00      0.00         1
  australia       0.00      0.00      0.00        20
    austria       0.00      0.00      0.00         4
    bahrain       0.00      0.00      0.00         4
 bangladesh       0.00      0.00      0.00         3
    belgium       0.00      0.00      0.00        16
    bermuda       0.00      0.00      0.00         2
    bolivia       0.00      0.00      0.00         7
   botswana       0.00      0.00      0.00         1
     brazil       0.00      0.00      0.00        14
   bulgaria       0.00      0.00      0.00         1
      burma       0.00      0.00      0.00         1
   cameroon       0.00      0.00      0.00         1
     canada       1.00      0.08      0.14        66
       chad       0.00      0.00      0.00         1
      chile       0.00      0.00      0.00         2
      china       1.00      0.14      0.25        14
   colombia       0.00      0.00      0.00         6
      costa       0.00      0.00      0.00         2
       cuba       0.00      0.00      0.00         2
     cyprus       0.00      0.00      0.00         4
    denmark       0.00      0.00      0.00         4
  dominican       0.00      0.00      0.00         1
       east       0.00      0.00      0.00         1
    ecuador       0.00      0.00      0.00         7
      egypt       0.00      0.00      0.00         3
         el       0.00      0.00      0.00         1
   ethiopia       0.00      0.00      0.00         1
       fiji       0.00      0.00      0.00         1
    finland       0.00      0.00      0.00         4
     france       0.89      0.36      0.52        22
      ghana       0.00      0.00      0.00         1
     greece       0.00      0.00      0.00         3
     guyana       0.00      0.00      0.00         1
      haiti       0.00      0.00      0.00         3
       hong       1.00      0.17      0.29        12
    hungary       0.00      0.00      0.00         3
    iceland       0.00      0.00      0.00         1
      india       0.00      0.00      0.00         6
  indonesia       0.00      0.00      0.00        11
       iran       0.00      0.00      0.00         5
       iraq       0.00      0.00      0.00         4
    ireland       0.00      0.00      0.00         2
     israel       0.00      0.00      0.00         3
      italy       1.00      0.44      0.62         9
      ivory       0.00      0.00      0.00         3
    jamaica       0.00      0.00      0.00         2
      japan       0.89      0.61      0.72        66
     jordan       0.00      0.00      0.00         2
      kenya       0.00      0.00      0.00         2
     kuwait       0.00      0.00      0.00         2
    lebanon       0.00      0.00      0.00         1
      libya       0.00      0.00      0.00         1
 luxembourg       0.00      0.00      0.00        10
 madagascar       0.00      0.00      0.00         2
     malawi       0.00      0.00      0.00         1
   malaysia       0.00      0.00      0.00         6
  mauritius       0.00      0.00      0.00         1
     mexico       0.00      0.00      0.00         4
    morocco       0.00      0.00      0.00         2
 mozambique       0.00      0.00      0.00         1
netherlands       0.00      0.00      0.00        15
        new       0.00      0.00      0.00        15
  nicaragua       0.00      0.00      0.00         2
    nigeria       0.00      0.00      0.00         2
      north       0.00      0.00      0.00         1
     norway       0.00      0.00      0.00         4
   pakistan       0.00      0.00      0.00         3
     panama       0.00      0.00      0.00         1
      papua       0.00      0.00      0.00         1
       peru       0.00      0.00      0.00         6
philippines       0.00      0.00      0.00        10
     poland       0.00      0.00      0.00         2
   portugal       0.00      0.00      0.00         2
      qatar       0.00      0.00      0.00         1
      saudi       0.00      0.00      0.00        10
     sierra       0.00      0.00      0.00         1
  singapore       0.00      0.00      0.00         8
    somalia       0.00      0.00      0.00         1
      south       0.00      0.00      0.00         9
      spain       0.00      0.00      0.00         6
        sri       0.00      0.00      0.00         4
      sudan       0.00      0.00      0.00         1
   suriname       0.00      0.00      0.00         1
     sweden       0.00      0.00      0.00        10
switzerland       0.00      0.00      0.00        19
      syria       0.00      0.00      0.00         1
     taiwan       0.00      0.00      0.00         9
   tanzania       0.00      0.00      0.00         3
   thailand       0.00      0.00      0.00         5
   trinidad       0.00      0.00      0.00         1
    tunisia       0.00      0.00      0.00         1
     turkey       0.00      0.00      0.00         7
        uae       0.00      0.00      0.00         7
     uganda       0.00      0.00      0.00         4
         uk       0.90      0.38      0.54        91
    uruguay       0.00      0.00      0.00         1
        usa       0.52      1.00      0.69       650
       ussr       0.00      0.00      0.00         6
  venezuela       0.00      0.00      0.00         5
    vietnam       0.00      0.00      0.00         1
       west       0.91      0.57      0.70        35
      yemen       0.00      0.00      0.00         1
 yugoslavia       0.00      0.00      0.00         7
      zaire       0.00      0.00      0.00         1
     zambia       0.00      0.00      0.00         3
   zimbabwe       0.00      0.00      0.00         2

avg / total       0.46      0.56      0.44      1371

NASTEPNY PLIK
Processing text dataset
Found 987 texts.
Loss 2.0881425920757937
Accuracy : 0.807497467072%
             precision    recall  f1-score   support

        acq       0.63      0.96      0.76       142
       alum       0.00      0.00      0.00         5
    austdlr       0.00      0.00      0.00         1
     barley       0.00      0.00      0.00         1
        bop       1.00      0.67      0.80         6
    carcass       1.00      0.20      0.33         5
      cocoa       1.00      1.00      1.00        11
    coconut       0.00      0.00      0.00         1
     coffee       1.00      1.00      1.00        15
     copper       1.00      0.75      0.86        16
       corn       0.00      0.00      0.00         5
     cotton       1.00      0.33      0.50         6
        cpi       0.90      0.90      0.90        10
        cpu       0.00      0.00      0.00         2
      crude       0.82      0.93      0.87        40
        dlr       0.00      0.00      0.00         8
       earn       0.96      0.99      0.97       269
          f       0.00      0.00      0.00         1
   fishmeal       0.00      0.00      0.00         1
       fuel       1.00      0.25      0.40         4
        gas       1.00      0.60      0.75         5
        gnp       1.00      0.93      0.97        15
       gold       1.00      0.64      0.78        11
      grain       0.62      0.90      0.73        49
  groundnut       0.00      0.00      0.00         1
       heat       1.00      0.33      0.50         3
        hog       1.00      1.00      1.00         3
    housing       1.00      0.80      0.89         5
     income       0.00      0.00      0.00         2
     instal       0.00      0.00      0.00         2
   interest       0.90      0.87      0.88        53
inventories       0.00      0.00      0.00         1
        ipi       1.00      0.67      0.80         6
       iron       1.00      0.38      0.55         8
        jet       0.00      0.00      0.00         1
       jobs       1.00      1.00      1.00         6
          l       0.00      0.00      0.00         1
       lead       0.83      1.00      0.91         5
        lei       0.00      0.00      0.00         2
  livestock       1.00      0.12      0.22         8
     lumber       0.00      0.00      0.00         2
       meal       1.00      0.33      0.50         3
      money       0.61      1.00      0.76        65
        nat       0.00      0.00      0.00         5
     nickel       0.00      0.00      0.00         1
      nzdlr       0.00      0.00      0.00         1
    oilseed       1.00      0.25      0.40         8
     orange       1.00      0.60      0.75         5
       palm       1.00      1.00      1.00         1
        pet       0.00      0.00      0.00         6
   platinum       0.00      0.00      0.00         1
    plywood       0.00      0.00      0.00         2
     potato       1.00      0.33      0.50         3
    propane       0.00      0.00      0.00         1
       rand       0.00      0.00      0.00         1
   rapeseed       1.00      1.00      1.00         2
   reserves       1.00      0.17      0.29         6
     retail       1.00      0.50      0.67         4
       rice       0.00      0.00      0.00         1
     rubber       1.00      0.25      0.40         4
  saudriyal       0.00      0.00      0.00         1
       ship       1.00      0.59      0.74        17
     silver       0.00      0.00      0.00         2
        soy       0.00      0.00      0.00         1
    soybean       0.00      0.00      0.00         1
        stg       1.00      0.50      0.67         4
  strategic       0.00      0.00      0.00         5
      sugar       1.00      0.85      0.92        13
    tapioca       0.00      0.00      0.00         1
        tea       0.00      0.00      0.00         2
        tin       1.00      0.20      0.33         5
      trade       0.71      1.00      0.83        40
        veg       1.00      0.83      0.91        12
      wheat       1.00      0.29      0.44         7
       wool       0.00      0.00      0.00         1
        wpi       1.00      1.00      1.00         6
        yen       1.00      0.17      0.29         6
       zinc       0.00      0.00      0.00         5

avg / total       0.79      0.81      0.77       987

NASTEPNY PLIK
Processing text dataset
Found 19997 texts.
Loss 2.404366651754672
Accuracy : 0.799519927989%
                          precision    recall  f1-score   support

             alt.atheism       0.67      0.74      0.70      1000
           comp.graphics       0.76      0.75      0.76      1000
 comp.os.ms-windows.misc       0.75      0.80      0.77      1000
comp.sys.ibm.pc.hardware       0.77      0.75      0.76      1000
   comp.sys.mac.hardware       0.89      0.80      0.84      1000
          comp.windows.x       0.89      0.81      0.85      1000
            misc.forsale       0.52      0.88      0.66      1000
               rec.autos       0.90      0.85      0.87      1000
         rec.motorcycles       0.94      0.91      0.92      1000
      rec.sport.baseball       0.90      0.88      0.89      1000
        rec.sport.hockey       0.92      0.94      0.93      1000
               sci.crypt       0.89      0.89      0.89      1000
         sci.electronics       0.92      0.66      0.77      1000
                 sci.med       0.94      0.83      0.88      1000
               sci.space       0.91      0.88      0.90      1000
  soc.religion.christian       0.64      0.92      0.76       997
      talk.politics.guns       0.71      0.90      0.79      1000
   talk.politics.mideast       0.82      0.91      0.86      1000
      talk.politics.misc       0.84      0.61      0.71      1000
      talk.religion.misc       0.76      0.28      0.41      1000

             avg / total       0.82      0.80      0.80     19997

