from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.linear_model import SGDClassifier
from sklearn.pipeline import Pipeline
from sklearn import metrics
from data_loader import process_dataset_20_newsgroups


def SVM(dataset_dir):
    # reuters jest preprocesowany tak, aby wygladal jak 20_newsgroups
    data, label_indices, label_names = process_dataset_20_newsgroups(dataset_dir)

    # pipeline (tokenizer => transformer => linear SVM classifier)
    text_clf = Pipeline([('vect', CountVectorizer()),
                         ('tfidf', TfidfTransformer()),
                         ('clf', SGDClassifier(loss='log', penalty='l2', alpha=1e-3, max_iter=10, random_state=42))])
    text_clf.fit(data, label_indices)

    # evaluate on test set
    predicted = text_clf.predict(data)
    print("Loss {0}".format(metrics.log_loss(label_indices, text_clf.predict_proba(data))))
    print("Accuracy : {}%".format(str(metrics.accuracy_score(label_indices, predicted))))
    print(metrics.classification_report(label_indices, predicted, target_names=label_names))
    # print("Score {0}".format(text_clf.score(data)))


for ds_s in ['reuters_places', 'reuters_topics', '20_newsgroup']:
    print("NASTEPNY PLIK")
    SVM(ds_s)
