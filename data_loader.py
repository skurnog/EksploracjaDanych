import os
import io
import sys
from bs4 import BeautifulSoup as BS
import time
import datetime
import pickle
from logger import Logger

BASE_DIR_REL_PATH = 'dataset'
DUMP_DIR_REL_PATH = 'dataset_parsed'


def dump_data(data_dir_name, texts=None, tag_name=None, labels=None, labels_index=None):
    def dump_to_file(obj, dir_path, fname):
        dump_file = open(os.path.join(dir_path, '%s.dump' % fname), 'wb')
        pickle.dump(obj, dump_file)
        dump_file.close()
        print('Dumped %s' % dump_file.name)

    print('Dumping objects')

    dump_datasetname_dir = os.path.join(DUMP_DIR_REL_PATH, data_dir_name)

    if not os.path.exists(dump_datasetname_dir):
        os.mkdir(dump_datasetname_dir)

    if texts is not None:
        dump_to_file(texts, dump_datasetname_dir, 'texts')

    if tag_name is not None:
        dump_tagname_dir = os.path.join(DUMP_DIR_REL_PATH, data_dir_name, tag_name)
        if not os.path.exists(dump_tagname_dir):
            os.mkdir(dump_tagname_dir)

        if labels is not None:
            dump_to_file(labels, dump_tagname_dir, 'labels')
        if labels_index is not None:
            dump_to_file(labels_index, dump_tagname_dir, 'labels_index')

    print('Done, bye.')


def process_dataset_20_newsgroups(data_dir_name, truncate_data=True):
    sys.stdout = Logger('process_dataset_20_newsgroups-%s.log' % datetime.datetime.now())

    data_dir = os.path.join(BASE_DIR_REL_PATH, data_dir_name)
    texts = []  # list of text samples
    labels_index = {}  # dictionary mapping label name to numeric id
    labels = []  # list of label ids
    dataset_len = 0

    print('Processing dataset %s' % data_dir_name)
    for name in sorted(os.listdir(data_dir)):
        path = os.path.join(data_dir, name)
        if os.path.isdir(path):
            label_id = len(labels_index)
            labels_index[name] = label_id
            for fname in sorted(os.listdir(path)):
                if fname.isdigit():
                    fpath = os.path.join(path, fname)
                    f = io.open(fpath, encoding='utf-8')
                    t = f.read()
                    if truncate_data:
                        i = t.find('\n\n')  # skip header
                        if 0 < i:
                            t = t[i:]
                    texts.append(t)
                    dataset_len += len(t)
                    f.close()
                    labels.append(label_id)

    print('Dataset statistics: texts: %d, labels: %d, overall corpus length in characters: %d.' %
          (len(texts), len(labels_index), dataset_len))

    dump_data(data_dir_name, texts=texts, tag_name='0', labels=labels, labels_index=labels_index)

    return texts, labels, labels_index


def process_dataset_blogs(data_dir_name):
    sys.stdout = Logger('process_dataset_blogs-%s.log' % datetime.datetime.now())

    def save_label(tag_value, labels_index, labels):
        label_id = len(labels_index)
        if labels_index.get(tag_value) is None:
            labels_index[tag_value] = label_id
        labels.append(label_id)
        return label_id

    data_dir = os.path.join(BASE_DIR_REL_PATH, data_dir_name)
    texts = []
    dataset_len = 0

    print('Processing dataset %s' % data_dir_name)
    files_count = len(os.listdir(data_dir))
    file_ind = 0

    execution_start = time.time()

    labels_list_categories = []
    labels_index_categories = []
    for name in sorted(os.listdir(data_dir)):
        path = os.path.join(data_dir, name)
        tags = name.split('.')[1:-1]
        if os.path.isfile(path):
            file_ind += 1
            input_content = io.open(path, encoding='utf-8').read()
            blog_posts = BS(input_content).find_all('post')
            blog_posts_len = len(blog_posts)

            print('Processing [%d/%d] %s' % (file_ind, files_count, path))

            for post_ind in range(blog_posts_len):
                if blog_posts[post_ind] is None:
                    print('WARNING: POST IS NONE')
                    continue
                post_text = blog_posts[post_ind].text.strip()
                texts.append(post_text)
                dataset_len += len(post_text)

                for i in range(len(tags)):
                    if i >= len(labels_index_categories):
                        labels_index_categories.append({})
                    if i >= len(labels_list_categories):
                        labels_list_categories.append([])
                    save_label(tags[i], labels_index_categories[i], labels_list_categories[i])

    execution_time_str = time.strftime("%H:%M:%S:%MS", time.gmtime(time.time() - execution_start))
    print('Processing finished in %s' % execution_time_str)
    print('Processed %d files' % files_count)
    print('Dataset statistics: texts: %d, corpus length: %d.' % (len(texts), dataset_len))
    for category_labels_ind in range(len(labels_list_categories)):
        print("For category %d labels %d" % (category_labels_ind, len(labels_list_categories[category_labels_ind])))

    for i in range(len(labels_list_categories)):
        dump_data(data_dir_name, tag_name=str(i), labels=labels_list_categories[i], labels_index=labels_index_categories[i])

    dump_data(data_dir_name, texts=texts)

    print('Processing dataset done, bye.')

    return texts, labels_list_categories, labels_index_categories


# TEST
if __name__ == '__main__':
    process_dataset_blogs('blogs_utf8')
