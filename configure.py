import os

keras_cfg = '''
{
    "floatx": "float32",
    "epsilon": 1e-07,
    "backend": "tensorflow",
    "image_data_format": "channels_last"
}
'''
cfg_keras_f = open(os.path.expanduser('~') + '/.keras/keras.json', 'w')
cfg_keras_f.write(keras_cfg)
cfg_keras_f.close()
