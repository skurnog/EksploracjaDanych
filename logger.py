import sys


class Logger(object):
    def __init__(self, output_filename):
        self.terminal = sys.stdout
        self.log = open(output_filename, 'w+')

    def write(self, message):
        self.terminal.write(message)
        self.log.write(message)  

    def flush(self):
        pass
