***Sieć 1***
20_newsgroup
3 warstwy konwolucyjne, pooling layer po każdej z nich
```python
model = Sequential()
model.add(Embedding(len(word_index) + 1, EMBEDDING_DIM, input_length=MAX_SEQUENCE_LENGTH))
model.add(Conv1D(128, 5, activation='relu'))
model.add(MaxPooling1D(5))
model.add(Conv1D(128, 5, activation='relu'))
model.add(MaxPooling1D(5))
model.add(Conv1D(128, 5, activation='relu'))
model.add(GlobalMaxPooling1D())
model.add(Dense(128, activation='relu'))
model.add(Dense(len(labels_index), activation='softmax'))
```

```
Epoch 1/10
15998/15998 [==============================] - 315s 20ms/step - loss: 2.6462 - acc: 0.1115 - val_loss: 2.2704 - val_acc: 0.1705
Epoch 2/10
15998/15998 [==============================] - 311s 19ms/step - loss: 2.1425 - acc: 0.2142 - val_loss: 2.0428 - val_acc: 0.2696
Epoch 3/10
15998/15998 [==============================] - 322s 20ms/step - loss: 1.8907 - acc: 0.3070 - val_loss: 1.8715 - val_acc: 0.3396
Epoch 4/10
15998/15998 [==============================] - 322s 20ms/step - loss: 1.5603 - acc: 0.4289 - val_loss: 1.6041 - val_acc: 0.4464
Epoch 5/10
15998/15998 [==============================] - 319s 20ms/step - loss: 1.2074 - acc: 0.5648 - val_loss: 1.4080 - val_acc: 0.5384
Epoch 6/10
15998/15998 [==============================] - 308s 19ms/step - loss: 0.9270 - acc: 0.6806 - val_loss: 1.2639 - val_acc: 0.6137
Epoch 7/10
15998/15998 [==============================] - 329s 21ms/step - loss: 0.6965 - acc: 0.7673 - val_loss: 1.2878 - val_acc: 0.6507
Epoch 8/10
15998/15998 [==============================] - 311s 19ms/step - loss: 0.5450 - acc: 0.8254 - val_loss: 1.4526 - val_acc: 0.6397
Epoch 9/10
15998/15998 [==============================] - 311s 19ms/step - loss: 0.4402 - acc: 0.8567 - val_loss: 1.2573 - val_acc: 0.6922
Epoch 10/10
15998/15998 [==============================] - 313s 20ms/step - loss: 0.3839 - acc: 0.8792 - val_loss: 1.2384 - val_acc: 0.7119
```

***Sieć 2***
20_newsgroup
6 warstw konwolucyjnych, pooling layer co dwie warstwy
```python
model = Sequential()
model.add(Embedding(len(word_index) + 1, EMBEDDING_DIM, input_length=MAX_SEQUENCE_LENGTH))
model.add(Conv1D(128, 5, activation='relu'))
model.add(Conv1D(128, 5, activation='relu'))
model.add(MaxPooling1D(5))
model.add(Conv1D(128, 5, activation='relu'))
model.add(Conv1D(128, 5, activation='relu'))
model.add(MaxPooling1D(5))
model.add(Conv1D(128, 5, activation='relu'))
model.add(Conv1D(128, 5, activation='relu'))
model.add(GlobalMaxPooling1D())
model.add(Dense(128, activation='relu'))
model.add(Dense(len(labels_index), activation='softmax'))
```

```
Epoch 1/10
15998/15998 [==============================] - 437s 27ms/step - loss: 2.7445 - acc: 0.0894 - val_loss: 2.5765 - val_acc: 0.1038
Epoch 2/10
15998/15998 [==============================] - 465s 29ms/step - loss: 2.3223 - acc: 0.1679 - val_loss: 2.1548 - val_acc: 0.1948
Epoch 3/10
15998/15998 [==============================] - 558s 35ms/step - loss: 2.0584 - acc: 0.2343 - val_loss: 1.9632 - val_acc: 0.2701
Epoch 4/10
15998/15998 [==============================] - 1256s 78ms/step - loss: 1.8091 - acc: 0.3150 - val_loss: 1.7577 - val_acc: 0.3318
Epoch 5/10
15998/15998 [==============================] - 1287s 80ms/step - loss: 1.5394 - acc: 0.4264 - val_loss: 1.5595 - val_acc: 0.4306
Epoch 6/10
15998/15998 [==============================] - 769s 48ms/step - loss: 1.3063 - acc: 0.5200 - val_loss: 1.4473 - val_acc: 0.5021
Epoch 7/10
15998/15998 [==============================] - 430s 27ms/step - loss: 1.0985 - acc: 0.6135 - val_loss: 1.4165 - val_acc: 0.5249
Epoch 8/10
15998/15998 [==============================] - 1141s 71ms/step - loss: 0.9198 - acc: 0.6760 - val_loss: 1.2631 - val_acc: 0.6139
Epoch 9/10
15998/15998 [==============================] - 1278s 80ms/step - loss: 0.7995 - acc: 0.7278 - val_loss: 1.5128 - val_acc: 0.5554
Epoch 10/10
15998/15998 [==============================] - 1262s 79ms/step - loss: 0.6813 - acc: 0.7677 - val_loss: 1.2040 - val_acc: 0.6519
```

REUTERS jest places => topics

BLOGS
Dataset statistics: 19320 files, texts: 284837, overall corpus length: 289933957

Found 427278 unique tokens.
Shape of data tensor: (284837, 1000)
Shape of label tensor: (284837, 41)
Train on 227869 samples, validate on 56967 samples

227869/227869 [==============================] - 205s 899us/step - loss: 1.2641 - acc: 0.6965 - val_loss: 1.2532 - val_acc: 0.6978
227869/227869 [==============================] - 198s 868us/step - loss: 1.1851 - acc: 0.7007 - val_loss: 1.2125 - val_acc: 0.7014
227869/227869 [==============================] - 202s 886us/step - loss: 1.1007 - acc: 0.7089 - val_loss: 1.2201 - val_acc: 0.6966
227869/227869 [==============================] - 207s 909us/step - loss: 0.9684 - acc: 0.7332 - val_loss: 1.3165 - val_acc: 0.6799
227869/227869 [==============================] - 207s 910us/step - loss: 0.8226 - acc: 0.7676 - val_loss: 1.4465 - val_acc: 0.6497
227869/227869 [==============================] - 207s 909us/step - loss: 0.7075 - acc: 0.7963 - val_loss: 1.6788 - val_acc: 0.6184
227869/227869 [==============================] - 207s 910us/step - loss: 0.6144 - acc: 0.8215 - val_loss: 1.8167 - val_acc: 0.6260
227869/227869 [==============================] - 207s 910us/step - loss: 0.5318 - acc: 0.8448 - val_loss: 1.9303 - val_acc: 0.6434
227869/227869 [==============================] - 207s 910us/step - loss: 0.4628 - acc: 0.8646 - val_loss: 2.1105 - val_acc: 0.5752
227869/227869 [==============================] - 207s 909us/step - loss: 0.4042 - acc: 0.8810 - val_loss: 2.1322 - val_acc: 0.6169