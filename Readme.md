*ccn.py* tworzy konwolucyjną sieć neuronową do klasyfikacji dokumentów tekstowych, trenuje ją na wybranym zbiorze testowym, wypisuje logi z treningu na standardowe wyjście.

*data_loader.py* parsuje zbiór danych, zapisuje wyniki na dysku.

*reuters_preprocessor.py* wykonuje preprocessing zbioru 'reuters', aby miał strukturę taką samą jak 20_newsgroup

*svm.py* 20_newsgroup klasyfikowany przez svm, do porównań z cnn

*configure.py* skonfiguruje **keras** do używania backendu **TensorFlow**.
Wystarczy pobrać biblioteki zawarte w requirements.txt i odpalić skrypt.

* W razie jakiś problemów z TensoFlow może pomóc skompilowanie najnowszej wersji z gita.
* Żeby działało na GPU trzeba zainstalować cudnn6 oraz cuda-8.0 (najnowsza wersja - 9.0 sprawia problemy na ten moment).

[Wyniki](/results.md)