import os
from bs4 import BeautifulSoup as BS


def parse_f(path, searched_tag, dest_path='reuters_parsed'):
    input_content = open(path).read()
    dest_path = os.path.join('dataset', dest_path)
    if not os.path.exists(dest_path):
        os.mkdir(dest_path)

    soup = BS(input_content)
    texts = soup.find_all('reuters')

    tags = {}
    for text in texts:
        # pprint(text.find('body'))
        tag = text.find(searched_tag)
        note = text.find('body')
        if tag.text is not "" and note is not None:
            tag = tag.find('d').text.split('-')[0]

            tag_count = tags.get(tag, 0)
            tags[tag] = tag_count + 1
            tag_dir_name = os.path.join(dest_path, tag)
            if not os.path.exists(tag_dir_name):
                os.mkdir(tag_dir_name)

            dest_filename = str(tag_count + 1)
            dest_file = open(os.path.join(dest_path, tag, dest_filename), 'w+')
            dest_file.write(note.text)
            dest_file.close()


def parse_set(tag, dest_path='reuters_parsed'):
    set_file_names = []
    for i in range(22):
        set_file_names.append('reut2-{0}.sgm'.format(str(i).zfill(3)))

    for file in set_file_names:
        try:
            parse_f(os.path.join('dataset', 'reuters', file), tag, dest_path)
        except UnicodeDecodeError:
            print('error')
            continue


# TEST
if __name__ == '__main__':
    parse_set('topics', dest_path='reuters_topics')
    parse_set('places', dest_path='reuters_places')

